using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModileControls : MonoBehaviour
{
    //[SerializeField] Canvas _mobileControllerCanvas;
    [SerializeField] AttackController _attackController;
    [SerializeField] PlayerController _playerController;

    public void AttackHandler()
    {
        _attackController.Attack();
    }

    public void JumpHandler()
    {
        _playerController.InitJump();
    }

    public void InteractHandler()
    {
        _playerController.Interact();
    }
}
