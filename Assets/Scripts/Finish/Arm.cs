using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm : MonoBehaviour
{
    private Finish_script _finish2;
    [SerializeField] private Animator _animator;
    void Start()
    {
        _finish2 = GameObject.FindGameObjectWithTag("Finish").GetComponent<Finish_script>();
    }

    public void FinishSetActive()
    {
        _finish2.FinishSetActive();
        _animator.SetTrigger("Activate");
    }
}
