using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish_script : MonoBehaviour
{
    [SerializeField] private GameObject _levelCompleteCanvas;
    [SerializeField] private GameObject _messageUI;
    // private Canvas _message; 
    private bool isActivated = false;
    private bool _isMessageUIShown;
    public void FinishLevel()
    {
        if (isActivated)
        {
            gameObject.SetActive(false);
            _levelCompleteCanvas.SetActive(true);
            Time.timeScale = 0f;

        }
        else
        {
            _isMessageUIShown = true;
            _messageUI.SetActive(_isMessageUIShown);
        }

    }


    public void FinishSetActive()
    {
        _isMessageUIShown = false;
        _messageUI.SetActive(_isMessageUIShown);
        isActivated = true;
    }

    public bool IsMessageUIShown
    {
        get => _isMessageUIShown;
    }

}
