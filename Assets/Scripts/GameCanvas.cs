using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameCanvas : MonoBehaviour
{
    [SerializeField] private GameObject _pauseCanvas;
 //   [SerializeField] private GameObject _gameCanvas;

    public void PauseHandler()
    {
        _pauseCanvas.SetActive(true);
        Time.timeScale = 0f;
      //  gameObject.SetActive(false);
    }
}
