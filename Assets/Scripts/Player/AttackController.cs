using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] AudioSource _attackSound;
    //[SerializeField] Canvas _joystickCanvas;

    private bool _isAttack;

    public bool IsAttack
    {
        get => _isAttack;
    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
         //   Attack();
        }


    }

    public void FinishAttack()
    {
        _isAttack = false;
    }

    public void Attack()
    {
        _isAttack = true;
        _attackSound.Play();
        _animator.SetTrigger("Attack");
    }
}
