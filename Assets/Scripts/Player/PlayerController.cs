using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Transform _playerModelTransform;
    [SerializeField] private AudioSource _jumpSound;
    [SerializeField] private FixedJoystick _fixedJoystick;

    Rigidbody2D _rb;
    private float _hor_speed;
    private Vector3 _scale;
    [SerializeField] private float _jump;
    private bool isground = false;
    private bool _isJumping = false;
    private bool _getFinish = false;
    private bool _getArm = false;
    private bool _isFacingRight = true;

    private Finish_script _finish1;
    private Arm _arm;

  

 

    //Анимация
    [SerializeField] private Animator _animator;


    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _scale = _rb.transform.localScale;
        _finish1 = GameObject.FindGameObjectWithTag("Finish").GetComponent<Finish_script>();
        _arm = GameObject.FindObjectOfType<Arm>();
       
    }

    private void Update()
    {
        _hor_speed = Input.GetAxis("Horizontal");
        _hor_speed = _fixedJoystick.Horizontal;
        _animator.SetFloat("SpeedX", Mathf.Abs(_hor_speed));

        if (Input.GetKey(KeyCode.Space))
        {
            InitJump();
        }

        if (Input.GetKeyDown(KeyCode.F)) Interact();      
    }

    private void FixedUpdate()
    {
        _rb.velocity = new Vector2(_hor_speed * _speed  * 50 * Time.fixedDeltaTime, _rb.velocity.y);

        if (_isJumping) Jumping();

        if (_hor_speed > 0 && !_isFacingRight)
        {
            _isFacingRight = true;
            FacingFlip(); 
        }
        else if (_hor_speed < 0 && _isFacingRight)
        {
            _isFacingRight = false;
            FacingFlip();
        }
    }

    public void InitJump()
    {
        if (!isground) return;
        _isJumping = true;
        isground = false;
        _jumpSound.Play();
    }
    private void Jumping()
    {
        _rb.velocity = new Vector2(_rb.velocity.x, 0);
        _rb.AddForce(new Vector2(0, _jump), ForceMode2D.Impulse);
        _isJumping = false;
    }

    public void Interact()
    {
        if(_getFinish) _finish1.FinishLevel();
        if(_getArm) _arm.FinishSetActive();
    }

    private void FacingFlip()
    {
        Vector3 Scale = _playerModelTransform.localScale;
        Scale.x *= -1;
        _playerModelTransform.localScale = Scale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("GND")) isground = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Finish"))
        {
            _getFinish = true;
        }
        if (collision.GetComponent<Arm>()) _getArm = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Finish")) _getFinish = false;
        if (collision.GetComponent<Arm>()) _getArm = false;
    }

  
   
}
