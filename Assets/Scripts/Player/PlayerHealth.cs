using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float _health;
    [SerializeField] private float _maxHealth;
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _gameOverCanvas;
    [SerializeField] private AudioSource _playerHitSound;
    // private Animator _animator;
    public void ReduseHealth(float damage)
    {
        _health -= damage;
        _playerHitSound.Play();
        InitHealth();
        _animator.SetTrigger("IsTakeHit");
        if (_health <= 0)
        {
            Die();

        }   
    }

    private void Start()
    {
        InitHealth();
    }

    private void InitHealth()
    {
        _healthSlider.value = _health / _maxHealth;
    }

    private void Die()
    {
        gameObject.SetActive(false);
        _gameOverCanvas.SetActive(true);
    }
}
