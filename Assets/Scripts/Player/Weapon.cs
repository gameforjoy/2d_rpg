using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private AudioSource _enmyHitSound;
    private AttackController _attackController;
    private float damage = 30f;
//    private EnemyHealth _enemyHealth;
    private void Start()
    {
        // _attackController = FindObjectOfType<AttackController>();
        // _attackController = GameObject.FindGameObjectWithTag("Player").GetComponent<AttackController>();
        _attackController = transform.root.GetComponent<AttackController>();
    }

 
    private void OnTriggerEnter2D(Collider2D collision)
    {
      //  EnemyController enemyController = collision.GetComponent<EnemyController>();
        EnemyHealth enemyHealth = collision.GetComponent<EnemyHealth>();
        if (_attackController.IsAttack && enemyHealth != null)
        {
            _enmyHitSound.Play();
            enemyHealth.ReduseHealth(damage);
            Debug.Log("Hit Enemy");

        }
         
       
    }
}
