using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float _waitForAttack;
    [SerializeField] private float _damage;
    [SerializeField] private float _circleRadius;
  //  [SerializeField] private float _damageDistance;
    [SerializeField] LayerMask _layerMask;
    [SerializeField] GameObject _currentHitObject;


  //  private EnemyController _enemyController;
    private float _curWaitForAttack;
    private bool _isEnemyAttack = true;
    

    private Vector2 _origin;
    private Vector2 _direction = Vector2.zero;

    private void Start()
    {
        _curWaitForAttack = _waitForAttack;
  //      _enemyController = GetComponent<EnemyController>();
    }

    /*
    private void OnCollisionStay2D(Collision2D collision)
    {
        PlayerHealth playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
        if (playerHealth != null && _isEnemyAttack)
        {
            playerHealth.ReduseHealth(_damage);
            _curWaitForAttack = _waitForAttack;
            _isEnemyAttack = false;
        }
    }
    */
    private void Update()
    {
        _origin = transform.position;
        RaycastHit2D hit = Physics2D.CircleCast(_origin, _circleRadius, _direction, 0, _layerMask);
        if (hit)
        {
            _currentHitObject = hit.transform.gameObject;
            if (_currentHitObject.CompareTag("Player") && _isEnemyAttack) Attack();
        }
        else
        {
            _currentHitObject = null;
        }



        if (!_isEnemyAttack)
        {
            _curWaitForAttack -= Time.deltaTime;
            if (_curWaitForAttack <= 0) 
                _isEnemyAttack = true;
        }
    }
    private void Attack()
    {
        Debug.Log("Attack");
        _isEnemyAttack = false;
        _curWaitForAttack = _waitForAttack;
        PlayerHealth playerHealth = _currentHitObject.GetComponent<PlayerHealth>();
        playerHealth.ReduseHealth(_damage);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_origin, _circleRadius);
    } 
}
