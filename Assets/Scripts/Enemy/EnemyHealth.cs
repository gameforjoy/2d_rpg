using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float _health;
    [SerializeField] private float _MaxHealth;
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Animator _animator;
    

    private void Start()
    {
     //   _animator = GetComponent<Animator>();
        InitHealth();

    }

 
    public void ReduseHealth(float damage)
    {
        _health -= damage;
        InitHealth();
        _animator.SetTrigger("IsTakeHit");
        if (_health <= 0)
        {
             Die();
        }
    }

    private void InitHealth()
    {
        _healthSlider.value = _health / _MaxHealth;
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }
}
