using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyController : MonoBehaviour
{
    [SerializeField] private float _patrolWidth;
     
    [SerializeField] private Transform _enemyModelTransform;
    [SerializeField] private float _patrolSpeed;
    [SerializeField] private float _chasingSpeed;
    [SerializeField] private float _timeToWait;
    [SerializeField] private float _timeToChase;
    [SerializeField] private float _minDistanceToPlayer;


    private float _curTimeToWait;
    private float _curTimeToChase;
    private Rigidbody2D _rb;
    private Transform _playerTransform;
    private Vector2 _rightBoundPoint;
    private Vector2 _leftBoundPoint;

    private Vector2 _deltaPosition;

    private bool _isFacingRight = false;
    private bool _isWait = false;

    private bool _isChasingPlayer = false;


     public bool IsFacingRight
     {
         get => _isFacingRight;
     }


    private void Start()
    {
        _curTimeToWait = _timeToWait;
        _curTimeToChase = _timeToChase;
        _rb = GetComponent<Rigidbody2D>();
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _leftBoundPoint = transform.position - Vector3.right * _patrolWidth;
        _rightBoundPoint = transform.position;

    }
    private void Update()
    {
        if (_isWait && !_isChasingPlayer) WaitingTimer();

        if (_isChasingPlayer) ChasingTimer();

        if (ShouldWait())  _isWait = true;
       

    }
    private void FixedUpdate()
    {
       // _deltaPosition = Vector2.right * _walkingSpeed * Time.fixedDeltaTime;
       
        if (_isChasingPlayer) ChasePlayer();
     
        if (!_isWait && !_isChasingPlayer) Patrol();
    }  
    
    private void Patrol()
    {
        _deltaPosition = Vector2.right * _patrolSpeed * Time.fixedDeltaTime;
        if (!_isFacingRight) _deltaPosition.x = -_deltaPosition.x;
        _rb.MovePosition((Vector2)transform.position + _deltaPosition);
    }
    private void ChasePlayer()
    {
        _deltaPosition = Vector2.right * _chasingSpeed * Time.fixedDeltaTime;
        float distance = transform.position.x - _playerTransform.position.x;
        if (distance > 0) _deltaPosition.x *= -1;

        if (distance > 0.1 && _isFacingRight) FlipDirection();
        if (distance < -0.1 && !_isFacingRight) FlipDirection();
        if (Mathf.Abs(distance) > _minDistanceToPlayer) _rb.MovePosition((Vector2)transform.position + _deltaPosition);
    }
    private void WaitingTimer()
    {
        _curTimeToWait -= Time.deltaTime;
        if (_curTimeToWait < 0)
        {
            _isWait = false;
            _curTimeToWait = _timeToWait;
            FlipDirection();
        }
    }

    private void ChasingTimer()
    {
        _curTimeToChase -= Time.deltaTime;
        if (_curTimeToChase < 0)
        {
            _isChasingPlayer = false;
            _curTimeToChase = _timeToChase;
        }
    }
   
    private bool ShouldWait()
    {

        bool _isOutOfRightBoundPoint = transform.position.x >= _rightBoundPoint.x && _isFacingRight;
        bool _isOutOfLeftBoundPoint = transform.position.x <= _leftBoundPoint.x && !_isFacingRight;
        return _isOutOfLeftBoundPoint || _isOutOfRightBoundPoint;
    }
    private void FlipDirection()
    {
        _enemyModelTransform.localScale = new Vector3(-_enemyModelTransform.localScale.x, _enemyModelTransform.localScale.y, _enemyModelTransform.localScale.z);
        _isFacingRight = !_isFacingRight;
    }

    public void StartChasingPlayer()
    {
        _isChasingPlayer = true;
        _curTimeToChase = _timeToChase;

    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_leftBoundPoint, _rightBoundPoint);
    }

}
